#!/bin/bash

set -e

if [ -z ${ENV} ]; then
  export ENV=production;
fi
asciidoctor -a attribute-missing=warn --failure-level=WARN \
  -a var-env-id=${ENV} \
  $@